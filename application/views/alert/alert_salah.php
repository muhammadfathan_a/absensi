<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <!-- General CSS Files -->
<link rel="stylesheet" href="<?= base_url().'assets/templates/vendor/sweetalert2/dist/sweetalert2.min.css' ?>">

</head>

<body>

  <script src="<?= base_url().'assets/templates/vendor/sweetalert2/dist/sweetalert2.min.js'?>"></script>

  <script type="text/javascript">
        Swal.fire({
            type: 'error',
            icon: 'error',
            title: 'Login Gagal!',
            text: ' Kode dosen atau password salah!',
            allowOutsideClick: false
        }).then((result) => {
            /* Read more about handling dismissals below */
            window.location.href = "../Login";
        });;
</script>

</body>

</html>


