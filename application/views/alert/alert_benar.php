<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
  <!-- General CSS Files -->
<link rel="stylesheet" href="<?= base_url().'assets/templates/vendor/sweetalert2/dist/sweetalert2.min.css' ?>">

</head>

<body>

  <script src="<?= base_url().'assets/templates/vendor/sweetalert2/dist/sweetalert2.min.js'?>"></script>

  <script type="text/javascript">
        Swal.fire({
            title: 'Berhasil Login, Selamat Datang',
            html: 'Anda akan di direct ke dashboard <b></b>.',
            timer: 2100,
            timerProgressBar: true,
            onBeforeOpen: () => {
                Swal.showLoading()
                timerInterval = setInterval(() => {
                    const content = Swal.getContent()
                    if (content) {
                        const b = content.querySelector('b')
                        if (b) {
                            b.textContent = Swal.getTimerLeft()
                        }
                    }
                }, 100)
            },
            onClose: () => {
                clearInterval(timerInterval)
            }
        }).then((result) => {
            /* Read more about handling dismissals below */
            window.location.href = "../app/homepages";
        });
</script>

</body>

</html>


