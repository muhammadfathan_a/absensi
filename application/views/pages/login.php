<body style="
	background-image: url(<?= base_url('assets/img/bg.png') ?>); 
	background-repeat: no-repeat; 
	background-position: center; 
	background-attachment: fixed;
	height: 100%; 
	background-size: cover;
">
    <!-- login content -->
    <div class="login-content py-7">
        <!-- Page content -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
					<div class="card border-0 mb-0" style="
						/* background-image: url(<?= base_url('assets/img/bg.png') ?>);  */
						height:100%; background-position:center; 
						border-radius:15px; 
						background-repeat: no-repeat; 
					">
                        <div class=" card-body px-lg-5 py-lg-5">
                            <div class="text-center text-dark mb-4 mt--4">
                                <div class="text-center">
                                    <img class="mb-3" src="<?= base_url('assets/img/UHAMKA/logo1.png') ?>" width="150" alt="">
                                    <div class="text-primary" style="font-size:15px;">
										<span style="font-weight:600;"> Sistem Absensi Perkuliahan</span>
                                    </div>
                            </div>

								<br> <div class="text-center" style="font-size:15px;"> Silahkan login terlebih dahulu </div>
                            <form role="form" class="mt-5" action="<?= base_url('Login/check')?>" method="POST">
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                        <input class="form-control text-primary" placeholder="Kode Dosen" type="text" name="kodedosen" id="kodedosen">
                                    </div>
                                </div>
                                <div class="form-group mb-5">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control text-primary" placeholder="Password" type="password" name="pwd" id="pwd">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button type="submit" class="btn btn-primary btn-lg btn-block">
                                        Login <i class="fas fa-arrow-right ml-1"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
						<div class="text-center mt-2 mb-4">
							<!-- Craft with <i class="fas fa-heart text-danger"></i> By. 
							<a href="https://bpti.uhamka.ac.id/internship-program/" target="_blank">
								Intership BPTI
							</a> -->
                            &copy; <?= date('Y'); ?>
                            <a href="https://bpti.uhamka.ac.id/" target="_blank">
								BPTI UHAMKA
							</a> <br>

						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
