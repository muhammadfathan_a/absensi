 <!-- =========================================================
 * Argon Dashboard PRO v1.1.0
 =========================================================

 * Product Page: https://www.creative-tim.com/product/argon-dashboard-pro
 * Copyright 2019 Creative Tim (https://www.creative-tim.com)

 * Coded by Creative Tim

 =========================================================

 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
  -->
 <!DOCTYPE html>
 <html>

 <body>
     <!-- Main content -->
     <div class="main-content">
         <!-- Header -->
         <div class="header bg-primary py-8">
             <div class="container">
                 <div class="header-body">
                     <div class="row align-items-center">
                         <div class="col-lg-6">
                             <div class="pr-5">
                                 <h1 class="display-2 text-white font-weight-bold mb-0">Absensi Akademik Mahasiswa</h1>
                                 <p class="text-white mt-4">Sistem absensi online yang dapat memudahkan urusan absensi atau kehadiran mahasiswa menjadi lebih mudah di saat covid 19.</p>
                                 <div class="mt-5">
                                     <a href="<?php echo base_url('Login') ?>" class="btn btn-neutral my-2">Login Disini</a>
                                 </div>
                             </div>
                         </div>
                         <div class="col-lg-6">
                             <div class="row pt-5">
                                 <div class="col-md-6">
                                     <div class="card">
                                         <div class="card-body">
                                             <div class="icon icon-shape bg-gradient-red text-white rounded-circle shadow mb-4">
                                                 <i class="ni ni-diamond"></i>
                                             </div>
                                             <h5 class="h3">Friendly UI / UX</h5>
                                             <p>Sistem ini dibuat dengan User Interface yang menarik dan mudah dipakai.</p>
                                         </div>
                                     </div>
                                     <div class="card">
                                         <div class="card-body">
                                             <div class="icon icon-shape bg-gradient-info text-white rounded-circle shadow mb-4">
                                                 <i class="ni ni-tablet-button"></i>
                                             </div>
                                             <h5 class="h3">Responsive</h5>
                                             <p>Website dibuat secara responsive sehingga dapat diakses di gadget.</p>
                                         </div>
                                     </div>
                                 </div>
                                 <div class="col-md-6 pt-lg-5 pt-4">
                                     <div class="card mb-4">
                                         <div class="card-body">
                                             <div class="icon icon-shape bg-gradient-success text-white rounded-circle shadow mb-4">
                                                 <i class="ni ni-time-alarm"></i>
                                             </div>
                                             <h5 class="h3">RealTime Pages</h5>
                                             <p>Semua proses yang di lakukan website di lakukan secara realtime.</p>
                                         </div>
                                     </div>
                                     <div class="card mb-4">
                                         <div class="card-body">
                                             <div class="icon icon-shape bg-gradient-warning text-white rounded-circle shadow mb-4">
                                                 <i class="ni ni-atom"></i>
                                             </div>
                                             <h5 class="h3">Dibuat BPTI</h5>
                                             <p>Dibuat oleh team BPTI yang terpercaya .</p>
                                         </div>
                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
             <div class="separator separator-bottom separator-skew zindex-100">
                 <svg x="0" y="0" viewBox="0 0 2560 100" preserveAspectRatio="none" version="1.1" xmlns="http://www.w3.org/2000/svg">
                     <polygon class="fill-default" points="2560 0 2560 100 0 100"></polygon>
                 </svg>
             </div>
         </div>
         <section class="py-6 pb-9 bg-default">
             <div class="row justify-content-center text-center mx-auto" style="width: 100%;">
                 <div class="col-md-6">
                     <h2 class="display-3 text-white">
					 	Main Fitur Sistem
					</h3>
					<p class="lead text-white">
						Website yang dibuat mengandalkan beberapa main fitur yang dapat 
						membantu para dosen untuk mengabsen mahasiswawnya dengan mudah melalui desktop 
						dan gadget handphone secara realtime
					</p>
                 </div>
             </div>
         </section>
         <section class="section section-lg pt-lg-0 mt--7">
             <div class="container">
                 <div class="row justify-content-center">
                     <div class="col-lg-12">
                         <div class="row mb-6">
                             <div class="col-lg-4">
                                 <div class="card card-lift--hover shadow border-0">
                                     <div class="card-body py-4" style="min-height: 270px;">
                                         <div class="icon icon-shape bg-gradient-primary text-white rounded-circle mb-4">
                                             <i class="ni ni-check-bold"></i>
                                         </div>
                                         <h4 class="h3 text-primary text-uppercase">Absensi Mahasiswa</h4>
                                         <p class="description mt-3">Absensi mahasiswa berupa aksi atau proses seperti ceklis mahasiswa yang hadir dan tidak hadir dalam perkuliahan</p>

                                     </div>
                                 </div>
                             </div>
                             <div class="col-lg-4">
                                 <div class="card card-lift--hover shadow border-0">
                                     <div class="card-body py-4" style="min-height: 270px;">
                                         <div class="icon icon-shape bg-gradient-success text-white rounded-circle mb-4">
                                             <i class="ni ni-archive-2"></i>
                                         </div>
                                         <h4 class="h3 text-success text-uppercase">Berita Acara </h4>
                                         <p class="description mt-3">Menyertakan berita acara apa saja yang telah di sampaikan ke mahasiswa yang nantinya akan menjadi laporan kegiatan mengajar bagi dosen.</p>

                                     </div>
                                 </div>
                             </div>
                             <div class="col-lg-4">
                                 <div class="card card-lift--hover shadow border-0">
                                     <div class="card-body py-4" style="min-height: 270px;">
                                         <div class="icon icon-shape bg-gradient-warning text-white rounded-circle mb-4">
                                             <i class="ni ni-books"></i>
                                         </div>
                                         <h4 class="h3 text-warning text-uppercase">Informasi Jadwal Dosen</h4>
                                         <p class="description mt-3">Mempermudah para dosen untuk dapat mengetahui kegiatan atau matakuliah apa yang sedang di ampu dan di tampilkan sesuai jamnya</p>

                                     </div>
                                 </div>
                             </div>
                         </div>
                     </div>
                 </div>
             </div>
         </section>
     </div>
	 <?php $this->load->view('layouts/footer') ?>
 </body>

 </html>
