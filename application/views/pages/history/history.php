<body>
	<!-- Main content -->
	<div class="main-content" style="
		background-image: url('<?= base_url('assets/img/bg.png') ?>');
		background-size: cover;
		background-position: center top;
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-position-y: 25px;
	">
		<!-- Header -->
		<div class="header py-6 align-items-center">
			<div class="fluid-container px-4">
				<div class="header-body">
					<!-- Card stats -->
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="card card-stats mb-0 pb-2">
								<!-- Card body -->
								<div class="card-body py-3 px-2">
									<div class="row mt-3 mb-2 px-4">
										<div class="col-sm-12 col-md-6 mb-2">
											<h3 class="card-title text-capitalize font-weight-bold mb-0"><i class="fas fa-book mr-3"></i>Pilih Matakuliah</h3>
										</div>
										<div class="col-sm-12 col-md-6 mb-2 text-right">
										</div>
									</div>
									<div class="bungkus-jadwal px-4" style="
									    max-height: none;
										height: 580px;
										overflow-y: scroll;
									">
										<div class="row pt-3 mx-auto">
										<?php if(!empty($dataJadwalAll)) { ?>
											<?php foreach ($dataJadwalAll as $dataJadwal) { ?>
												<div class="col-md-6 px-2" id="getScheduleActive">
												<a href = "<?= base_url('app/history_detail?id='.$dataJadwal['IDJADWAL']) ?>" style="width: 100%;">
													<div class="card mb-3 shadow bg-white text-dark-all">
														<!-- Card body -->
														<div class="card-body">
															<div class="row align-items-center">
																<div class="col-sm-12 col-md-8">
																	<h5 class="card-title text-uppercase mb-0">
																		<?= $dataJadwal['matkul'] ?> (<?= $dataJadwal['KELAS'] ?>) <br><br>
																		<small>
																			<b><?= $dataJadwal['HARI'] ?> (<?= $dataJadwal['MULAI'] ?>-<?= $dataJadwal['SAMPAI'] ?>) <br>
																			<?= $dataJadwal['SINGKATAN']?> <br>
																			<?= $dataJadwal['NAMAPROGDI']?> </b>
																		</small>
																	</h5>
																</div>
																<div class="col-sm-12 col-md-4 float-right">
																	<div class="row mx-auto">
																		<div class="col-12 px-1 text-right mb-2 text-right">
																			<span style="display: inline-table; font-size: 13px;">
																				<span class="btn-inner--icon"><i class="ni ni-building"></i></span>
																				<span class="btn-inner--text"> <?= $dataJadwal['RUANG'] ?></span>
																			</span>
																		</div>
																		<div class="col-12 px-0 text-right">
																				<button class="mx-auto btn btn-sm btn-success my-2 ml-2"  >
																					<span class="btn-inner--text">
																						Lihat History Absen
																						<!-- <i class="far fa-thumbs-up"></i> -->
																					</span>
																				</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													</a>
												</div>
											<?php } 
										} else{ 
											echo '									<div class="row px-4 mb-3 mt-3">
											<div class="col-sm-12 col-md-12 mb-2">
												<h3 class="card-title text-capitalize font-weight-bold mb-0">
														TIDAK ADA JADWAL														
												</h3>
											</div>
										</div>';	

										}?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $this->load->view('layouts/footer') ?>
	</div>
</body>
