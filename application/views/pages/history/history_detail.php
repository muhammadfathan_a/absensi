<body>
	<!-- Main content -->
	<div class="main-content" style="
		background-image: url('<?= base_url('assets/img/bg.png') ?>');
		background-size: cover;
		background-position: center top;
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-position-y: 25px;
	">
		<!-- Header -->
		<div class="header py-6 align-items-center">
			<div class="fluid-container px-4">
				<div class="header-body">
					<!-- Card stats -->
					<div class="row">
						<div class="col-sm-12 col-md-12">
							<div class="card card-stats mb-0 pb-2">
								<!-- Card body -->
								<div class="card-body py-3 px-2">
									<div class="row mt-3 mb-2 px-4">
										<div class="col-sm-12 col-md-6 mb-2">
											<h3 class="card-title text-capitalize font-weight-bold mb-0"><i class="fas fa-book mr-3"></i>Pilih History Absensi</h3>
										</div>
										<div class="col-sm-12 col-md-6 mb-2 text-right">
										</div>
									</div>
									<div class="bungkus-jadwal px-4" style="
									    max-height: none;
										height: 580px;
										overflow-y: scroll;
									">
										<div class="row pt-3 mx-auto">
											<?php if(!empty($dataJadwalAll)) { ?>
											<?php foreach ($dataJadwalAll as $dataJadwal) { ?>
												<div class="col-md-6 px-2" id="getScheduleActive">
												<a href = "<?= base_url('app/history_pertemuan?id='.$dataJadwal['IDJADWAL'].'&pertemuan='.$dataJadwal['PERTEMUAN']) ?>" style="width: 100%;">
													<div class="card mb-3 shadow bg-white text-dark-all">
														<!-- Card body -->
														<div class="card-body">
															<div class="row align-items-center">
																<div class="col-sm-12 col-md-8">
                                                                <h5 class="card-title text-uppercase mb-0">
																		Pertemuan <?= $dataJadwal['PERTEMUAN'] ?>  <br><br>
																		<small>
                                                                            <?php
                                                                                $tglabsen = date('Y-m-d', strtotime($dataJadwal['TANGGAL']));
                                                                            ?>
																			Tanggal Absen : <?= longdate_indo($tglabsen) ?>
																		</small><br>
                                                                        <small>
                                                                            Jam Absen : <?= $dataJadwal['WaktuAbsen'] ?> WIB
                                                                        </small><br>
                                                                        <small>
                                                                            Pokok Bahasan :
                                                                            <?php 
                                                                                if($dataJadwal['PokokBahasan'] == '' ){
                                                                                    echo '-';
                                                                                }else{
                                                                                    echo $dataJadwal['PokokBahasan'];
                                                                                }
                                                                            ?> 
                                                                        </small>
																	</h5>
																</div>
															</div>
														</div>
													</div>
													</a>
												</div>
											<?php } ?>
											<?php } else {?>
												<h3 class="card-title text-capitalize font-weight-bold mb-0">
															Belum Ada Pertemuan														
													</h3>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $this->load->view('layouts/footer') ?>
	</div>
</body>
