<?php date_default_timezone_set('Asia/Jakarta'); ?>

<?php $per = $this->input->get('pertemuan'); ?>
<?php if( $per >  $JadwalIdentity[0]['JmlAbsen']){ ?>
    <script> 
            alert("History pertemuan belum ada!"); 
            window.location = "<?= base_url('app/history')?>";
    </script>
<?php } else {  ?>
<div class="container-fluid pt-5 bg-white" style="zoom: 80%;">
    <div class="row px-3 pb-5 card-wrapper">

        <div class="col-sm-12 col-md-3" style="order: 2;">
            <div class="data-berita-acara row mx-auto align-items-center" style="width: 100%;">
                <div class="col-md-9 col-sm-12 px-0 mb-3">
                    <h2 class="text-dark d-inline-block mb-3">
                        Berita Acara 
                    </h2>
                </div>
                <div class="col-md-12 col-sm-12 px-0 text-right">
                    <?php
                        $idjadwal = $this->input->get('id');
                        $pertemuan = $this->input->get('pertemuan');
                    ?>
                    <form method="POST" action="<?= base_url('jadwal/save') ?>">
                        <textarea name = 'berita' rows="3" class="form-control shadow p-3" style="border: none !important; font-size: 16px; width: 100%;" placeholder="Pembahasan Kuliah" readonly><?= $HistoryMatkulDetail[0]['PokokBahasan'] ?></textarea>
                    </form>
                </div>
            </div>
            <div class="data-info-absensi row mx-auto mt-4 align-items-center" style="width: 100%;">
                <div class="col-md-9 col-sm-12 px-0">
                    <h2 class="text-dark d-inline-block mb-2">
                        Informasi Absensi
                    </h2>
                </div>
                <div class="card" style="width: 100%;">
                    <div class="card-body p-0">
                        <ul class="list-group">
                        <li class="list-group-item" style="border-radius: 0px !important;">
                                Jumlah Mahasiswa
                                <span id="attendSum" style="font-weight: 700;"> <?= $JadwalIdentity[0]['PESERTA'] ?> </span> Orang
                            </li>
                            <li class="list-group-item" style="border-radius: 0px !important;" >
                                Hadir berjumlah
                                <span  style="font-weight: 700;"><?= $countHadirHistory[0]['hadir'] ?></span> Orang
                            </li>
                            <li class="list-group-item">
                                Tidak Hadir berjumlah
                                <span  style="font-weight: 700;"><?= $countTidakHadirHistory[0]['tidakhadir'] ?></span> Orang
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-sm-12 col-md-9" style="order: 1;">
            <div class="data-mahasiswa row mx-auto">
                <div class="row mb-3 mx-auto" style="width: 100%;">

                    <div class="col-md-9 col-sm-12 px-0 align-items-center">
                        <h2 class="text-dark" style="padding-top: 5px;">
                            <?= $JadwalIdentity[0]['NAMADOSEN'] ?><br>
                            <?= $JadwalIdentity[0]['NAMAMK'] ?> 
                            (Pertemuan <?= $HistoryMatkulDetail[0]['PERTEMUAN'] ?>)  <br>
                            <small>
                                <?php
                                    $tglabsen = date('Y-m-d', strtotime($HistoryMatkulDetail[0]['TANGGAL']));
                                ?>
                                Tanggal Absen : <?= longdate_indo($tglabsen) ?>
                            </small><br>
                            <small>
                                Jam Absen : <?= $HistoryMatkulDetail[0]['WaktuAbsen'] ?> WIB
                            </small><br>
                        </h2>
                    </div>
                    <!-- <div class="col-md-3 col-sm-12 text-right px-0 d-flex align-items-center">
                        <input type="text" class="form-control shadow-none" name="nama_mhs" placeholder="Cari Mahasiswa.." style="border: none; outline: none; border-bottom: 1px solid #eee;">

                        <button disabled="disabled" class="btn btn-white shadow-none ml-1" style="border: none; outline: none;">
                            <i class="fas fa-search"></i>
                        </button>
                    </div> -->
                </div>
                <div id="data-absen" class="data-mhs row py-3 rounded jadwal_detail" style="overflow-y: auto; max-height: 533px; background: #eee;">
                <?php foreach($JadwalDetail as $jadwaldetail) { ?>
                    <div class="col-xl-3 col-md-6 col-sm-12 ">
                        <div class="card card-primary px-0 rounded card-stats shadow" style="margin-bottom: 15px;">
                            <!-- Card body -->
                            <div class="card-body px-0">
                                <div class="row" style="width: 100%; margin: auto; zoom: 110%;">
                                    <!-- atas -->
                                    <div class="col-sm-12 col-md-12">
                                        <h4 class="capitalize font-weight-500 mb-2">
                                            <?= $jadwaldetail['NAMA'] ?>
                                        </h4>
                                    </div>
                                    <!-- bawah -->
                                    <div class="row d-flex" style="width: 100%; margin: auto;">
                                        <div class="col-sm-12 col-md-6">
                                            <h5 class="text-mute mb-0 font-weight-300">
                                            <?= $jadwaldetail['NIM'] ?>
                                            </h5>
                                        </div>
                                    </div>
                                    <?php $pert = $this->input->get('pertemuan') ?>
                                    <?php if( $jadwaldetail['K'.$pert] == '0') { ?>
                                        <div class="col-sm-12 col-md-6 text-right float-right">
                                            <div class="custom-control custom-switch" style="padding-left: 2.5rem; zoom: 90%;">
                                                <input type="checkbox" name="attend_type_<?= $jadwaldetail['NIM'] ?>" class="custom-control-input bg-success btn-absen" data-nim="<?= $jadwaldetail['NIM'] ?>" data-idjadwal="<?= $jadwaldetail['IDJADWAL'] ?>" checked id="customSwitch_<?= $jadwaldetail['NIM'] ?>" disabled>
                                                <label class="custom-control-label" for="customSwitch_<?= $jadwaldetail['NIM'] ?>">
                                                    <small class="mx-2">Hadir</small>
                                                </label>
                                            </div>
                                        </div>
                                    <?php }else if($jadwaldetail['K'.$pert] == '1') { ?>
                                        <div class="col-sm-12 col-md-6 text-right float-right">
                                            <div class="custom-control custom-switch" style="padding-left: 2.5rem; zoom: 90%;">
                                                <input type="checkbox" name="attend_type_<?= $jadwaldetail['NIM'] ?>" class="custom-control-input bg-success btn-hadir" data-nim="<?= $jadwaldetail['NIM'] ?>" data-idjadwal="<?= $jadwaldetail['IDJADWAL'] ?>" id="customSwitch_<?= $jadwaldetail['NIM'] ?>" disabled>
                                                <label class="custom-control-label" for="customSwitch_<?= $jadwaldetail['NIM'] ?>">
                                                    <small class="mx-2">Tidak Hadir</small>
                                                </label>
                                            </div>
                                        </div>

                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php } ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->load->view('layouts/footer') ?>
    <?php $this->load->view('templates/footer') ?>

</div>

<?php }  ?>



