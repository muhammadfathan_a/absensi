<style>
    .note-modal-footer {
        height: 60px !important;
        padding: 10px;
        text-align: center;
    }
</style>

<div class="main-content" id="panel">
    <div class="container mt-5 pt-3 mb-15">
        <div class="card">
            <form enctype="multipart/form-data" action="<?= base_url('admin/save_pengumuman') ?>" method="post" >
                <div class="card-header mb-0 p-4 bg-white">
                    <h4 class="mb-0"> 
                        <i class="fas fa-edit mr-2"></i>
                        Form Pengumuman 
                    </h4>
                </div>
                <div class="card-body">
                    <input type="hidden" name="id_pengumuman" value="<?= $pengumuman->id_pengumuman ?>">
                    <!-- <input type="hidden" name="id_pengumuman"> -->
                    <textarea id="summernote" name="pengumuman" style="z-index: 999 !important; height: 300px;"><?= $pengumuman->pengumuman ?></textarea>
                    <!-- <small class="text-dark">*maksimal upload hanya 1 gambar.</small> -->
                </div>
                <div class="card-footer mb-0 p-4 bg-white">
                    <div class="row">
                        <div class="col-md-6 text-left">
                            <div class="custom-control custom-switch">
                                <input type="checkbox" <?= $pengumuman->status == 1 ? 'checked' : '' ?> 
                                class="custom-control-input" id="customSwitch1" name="status">

                                <!-- <input type="checkbox" 
                                class="custom-control-input" id="customSwitch1" name="status"> -->

                                <label class="custom-control-label" for="customSwitch1">
                                    Status Pengumuman
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 text-right ">
                            <button class="btn btn-primary" type="submit">
                                Simpan
                            </button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>



<?php $this->load->view('templates/footer') ?>

<script>
    $(document).ready(function(){
        $('#summernote').summernote({
            placeholder: 'Ketik isi pengumuman',
            tabsize: 2,
            height: 400,
            callbacks: {
                onImageUpload: function(image) {
                    uploadImage(image[0]);
                },
                onMediaDelete : function(target) {
                    deleteImage(target[0].src);
                }
            }
        });
    });
</script>