<body>
	<!-- Main content -->
	<div class="main-content" style="
		background-image: url('<?= base_url('assets/img/bg.png') ?>');
		background-size: cover;
		background-position: center top;
		background-attachment: fixed;
		background-repeat: no-repeat;
		background-position-y: 25px;
	">
		<!-- Header -->
		<div class="header py-6 align-items-center">
			<div class="fluid-container px-4">
				<div class="header-body">
					<!-- Card stats -->
					<div class="row">

						<div class="col-sm-12 col-md-4">
							<div class="card card-stats" style="max-height: 695px;">
								<!-- Card body -->
								<div class="card-body pt-3 pb-1 px-3">
									<div class="row px-4 mb-3 mt-3">
										<div class="col-sm-12 col-md-12 mb-2">
											<h3 class="card-title text-capitalize font-weight-bold mb-0">
												<i class="ni ni-briefcase-24 mr-3"></i>
												Jadwal Hari Ini <br> 
											</h3>
											<?= longdate_indo(date('Y-m-d')) ?> <br>

											Jam : <?= date('H:i') ?>

										</div>
									</div>
									<div class="row py-3" style="overflow-y: auto;">

										<div class="row mx-auto px-2" style="width: 100%;">
										<?php if(!empty($dataJadwalPerDay)) { ?>

										<?php  foreach($dataJadwalPerDay as $jadwalDay) {?>
											
											<div class="col-md-12" id="getScheduleActive">
													<?php 
													$now = strtotime(date('H:i'));
													$daynow_kiri = onlyday_indo(date('Y-m-d'));
													$start = strtotime($jadwalDay['MULAI']);
													$mid = strtotime($jadwalDay['SAMPAI']);

													$hari_kiri = str_replace(' ','', $jadwalDay['HARI']);
													$daynow_kiri = str_replace(' ', '', $daynow_kiri);


														if($now >= $start && $now <= $mid && ($daynow_kiri == $hari_kiri)){
													?>
													<a href = "<?= base_url('app/dashboard?id='.$jadwalDay['IDJADWAL'].'&pertemuan='.$jadwalDay['JmlAbsen']) ?>" style="width: 100%;">
													<?php } ?>
													<div class="card mb-3 bg-white text-dark-all">
														<!-- Card body -->
														<div class="card-body">
															<div class="row align-items-center">
																<div class="col-sm-12 col-md-8">
																	<h5 class="card-title text-uppercase mb-0">
																	<?= $jadwalDay['matkul'] ?> (<?= $jadwalDay['KELAS'] ?>) <br><br>

																		<small>
																		<?= $jadwalDay['HARI'] ?> (<?= $jadwalDay['MULAI'] ?>-<?= $jadwalDay['SAMPAI'] ?>) <br>
																			<!-- Jumlah Mahasiswa : 20 Orang -->
																		</small>
																	</h5>
																</div>
																<div class="col-sm-12 col-md-4 float-right">
																	<div class="row mx-auto text-center">
																		<div class="col-12 px-1 text-right mb-2 text-right">
																			<span style="display: inline-table; font-size: 13px;">
																				<span class="btn-inner--icon"><i class="ni ni-building"></i></span>
																				<span class="btn-inner--text"> <?= $jadwalDay['RUANG'] ?></span>
																			</span>
																		</div>
																		
																		<?php 
																		
																		$now = strtotime(date('H:i'));
																		$daynow_kiri = onlyday_indo(date('Y-m-d'));
																		$start = strtotime($jadwalDay['MULAI']);
																		$mid = strtotime($jadwalDay['SAMPAI']);

																		$hari_kiri = str_replace(' ','', $jadwalDay['HARI']);
																		$daynow_kiri = str_replace(' ', '', $daynow_kiri);

																		if($now >= $start && $now <= $mid && ($daynow_kiri == $hari_kiri)){
																		?>
																		<div class="col-12 px-0 text-right">
																				<button class="mx-auto btn btn-sm btn-success my-2 ml-2"  >
																					<span class="btn-inner--text">
																						Sedang Berlangsung 
																						<!-- <i class="far fa-thumbs-up"></i> -->
																					</span>
																				</button>
																		</div>
																		<?php } else {?>
																			<div class="col-12 px-0 text-right">
																				<button class="mx-auto btn btn-sm btn-danger my-2 ml-2"  >
																					<span class="btn-inner--text">
																						Belum Berlangsung 
																						<!-- <i class="far fa-thumbs-up"></i> -->
																					</span>
																				</button>
																		</div>
																		<?php } ?>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</a>
											</div>
										<?php } ?>
										<?php } else{
												echo '									<div class="row px-4 mb-3 mt-3">
												<div class="col-sm-12 col-md-12 mb-2">
													<h3 class="card-title text-capitalize font-weight-bold mb-0">
															TIDAK ADA JADWAL														
													</h3>
												</div>
											</div>';
										 }?>
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-sm-12 col-md-8">
							<div class="card card-stats mb-0 pb-2">
								<!-- Card body -->
								<div class="card-body py-3 px-2">
									<div class="row mt-3 mb-2 px-4">
										<div class="col-sm-12 col-md-6 mb-2">
											<h3 class="card-title text-capitalize font-weight-bold mb-0"><i class="fas fa-book mr-3"></i>Jadwal Keseluruhan</h3>
										</div>
										<div class="col-sm-12 col-md-6 mb-2 text-right">
										</div>
									</div>
									<div class="bungkus-jadwal px-4" style="
									    max-height: none;
										height: 580px;
										overflow-y: scroll;
									">
										<div class="row pt-3 mx-auto">
											<?php foreach ($dataJadwalAll as $dataJadwal) { ?>
												<div class="col-md-6 px-2" id="getScheduleActive">
												<a href = "<?= base_url('app/dashboard?id='.$dataJadwal['IDJADWAL'].'&pertemuan='.$dataJadwal['JmlAbsen']) ?>" style="width: 100%;">
													<div class="card mb-3 shadow bg-white text-dark-all">
														<!-- Card body -->
														<div class="card-body">
															<div class="row align-items-center">
																<div class="col-sm-12 col-md-8">
																	<h5 class="card-title text-uppercase mb-0">
																		<?= $dataJadwal['matkul'] ?> (<?= $dataJadwal['KELAS'] ?>) <br><br>
																		<small>
																			<?= $dataJadwal['HARI'] ?> (<?= $dataJadwal['MULAI'] ?>-<?= $dataJadwal['SAMPAI'] ?>) <br>
																			<!-- Jumlah Mahasiswa : 20 Orang -->
																		</small>
																	</h5>
																</div>
																<div class="col-sm-12 col-md-4 float-right">
																	<div class="row mx-auto">
																		<div class="col-12 px-1 text-right mb-2 text-right">
																			<span style="display: inline-table; font-size: 13px;">
																				<span class="btn-inner--icon"><i class="ni ni-building"></i></span>
																				<span class="btn-inner--text"> <?= $dataJadwal['RUANG'] ?></span>
																			</span>
																		</div>
																		<div class="col-12 px-0 text-right">
																				<button class="mx-auto btn btn-sm btn-success my-2 ml-2"  >
																					<span class="btn-inner--text">
																						Sedang Berlangsung 
																						<!-- <i class="far fa-thumbs-up"></i> -->
																					</span>
																				</button>
																		</div>
																	</div>
																</div>
															</div>
														</div>
													</div>
													</a>
												</div>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php $this->load->view('layouts/footer') ?>
	</div>
</body>
