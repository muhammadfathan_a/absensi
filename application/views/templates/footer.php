</div>

    <!-- Core -->
    <script src="<?= base_url('assets/templates/vendor/jquery/dist/jquery.min.js') ?>"></script>
    <script src="<?= base_url('assets/jquery-validation/jquery.validate.min.js')?>"></script>
    <script src="<?= base_url('assets/templates/vendor/bootstrap/dist/js/bootstrap.bundle.min.js') ?>"></script>
    <script src="<?= base_url('assets/templates/vendor/js-cookie/js.cookie.js') ?>"></script>
    <script src="<?= base_url('assets/templates/vendor/jquery.scrollbar/jquery.scrollbar.min.js') ?>"></script>
    <script src="<?= base_url('assets/templates/vendor/jquery-scroll-lock/dist/jquery-scrollLock.min.js') ?>"></script>
    
    <!-- Optional JS -->
    <script src="<?= base_url('assets/templates/vendor/onscreen/dist/on-screen.umd.min.js') ?>"></script>
    <script src="<?= base_url('assets/templates/vendor/chart.js/dist/Chart.min.js') ?>"></script>
    <script src="<?= base_url('assets/templates/vendor/chart.js/dist/Chart.extension.js') ?>"></script>

    <!-- Argon JS -->
    <script src="<?= base_url('assets/templates/js/argon.js') ?>"></script>

    <!-- Demo JS - remove this in your project -->
    <script src="<?= base_url('assets/templates/js/demo.min.js') ?>"></script>

	<!-- Custom JS -->
	<script src="<?= base_url('assets/templates/js/login.js') ?>"></script>
	<script src="<?= base_url('assets/templates/js/logout.js') ?>"></script>
	<!-- <script src="<?= base_url('assets/templates/js/attend.switch.js') ?>"></script>	 -->
	
    <!-- swal cdn -->
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>

    <script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.js"></script>

</body>

</html>
