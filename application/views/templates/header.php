<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Aplikasi absensi online Universitas Muhammadiyah Prof. Dr. Hamka">
	<meta name="author" content="BPTI UHAMKA">
	<title> Sistem Absensi Perkuliahan | UHAMKA </title>

	<link rel="icon" href="<?= base_url('assets/templates/img/brand/favicon.png') ?>" type="image/png">

	<link rel="stylesheet" href="<?= base_url('assets/templates/vendor/nucleo/css/nucleo.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= base_url('assets/templates/vendor/@fortawesome/fontawesome-free/css/all.min.css') ?>" type="text/css">

	<link rel="stylesheet" href="<?= base_url('assets/templates/css/argon.css?v=1.1.0') ?>" type="text/css">
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">

	<!-- Start Alexa Certify Javascript -->
	<script type="text/javascript">
	_atrk_opts = { atrk_acct:"e8yqx15yOB20t5", domain:"uhamka.ac.id",dynamic: true};
	(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://certify-js.alexametrics.com/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
	</script>
	<noscript><img src="https://certify.alexametrics.com/atrk.gif?account=e8yqx15yOB20t5" style="display:none" height="1" width="1" alt="" /></noscript>
	<!-- End Alexa Certify Javascript -->  

	<!-- Summernote -->
    <link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-lite.min.css" rel="stylesheet">
</head>
<body>
    <!-- Main content -->
    <div class="main-content">
