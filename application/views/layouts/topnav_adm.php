<!-- Topnav -->
<nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse float-right" id="navbarSupportedContent">
            <ul class="navbar-nav align-items-center mx-auto ml-md-0">
                <li class="nav-item">
                    <a class="nav-link px-0 text-white" href="<?= base_url('admin/pengumuman') ?>" style="font-size: 16px;">
                        <span style="font-weight: 100;">
                        <span class="btn-inner--icon"><i class="ni ni-calendar-grid-58"></i> &nbspSetting Pengumuman
                        </span>
                    </a>
                </li>
            </ul>

            <ul class="navbar-nav align-items-center mx-auto mr-md-0">
                <li class="nav-item">
                    <a class="nav-link pr-0" href=<?= base_url('admin/login')?>>
                        <div class="media align-items-center">
                            <div class="media-body ml-3 d-lg-block">
                                <button class="btn btn-icon btn-secondary" type="button">
                                    <span class="btn-inner--icon"><i class="ni ni-circle-08 mr-2"></i></span>
                                    <span class="mb-0 text-sm font-weight-bold">
                                    Logout
                                    </span>
                                </button>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>