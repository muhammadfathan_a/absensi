<!-- Topnav -->
<nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse float-right" id="navbarSupportedContent">
            <ul class="navbar-nav align-items-center mx-auto ml-md-0">
                <li class="nav-item">
                    <a class="nav-link px-0 text-white" href="<?= base_url('app/homepages') ?>" style="font-size: 16px;">
                        <span style="font-weight: 100;">
                        <span class="btn-inner--icon"><i class="ni ni-calendar-grid-58"></i> &nbspJadwal
                        </span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav align-items-center mx-auto ml-md-0">
                <li class="nav-item">
                    <a class="nav-link px-0 text-white" href="<?= base_url('app/history') ?>" style="font-size: 16px;">
                        <span style="font-weight: 100;">
                        <span class="btn-inner--icon"><i class="ni ni-bullet-list-67"></i> &nbspHistory Absensi
                        </span>
                    </a>
                </li>
            </ul>
            <?php if (isset($pengumuman->status) && $pengumuman->status  == 1) { ?>
            <ul class="navbar-nav align-items-center mx-auto ml-md-0">
                <li class="nav-item">
                    <a class="nav-link px-0 text-white" href="#" style="font-size: 16px;" data-toggle="modal" data-target="#exampleModalCenter">
                        <span style="font-weight: 100;">
                        <span class="btn-inner--icon"><i class="ni ni-notification-67"></i> &nbspLihat Pengumuman
                        </span>
                    </a>
                </li>
            </ul>
            <?php } ?>
            <ul class="navbar-nav align-items-center mx-auto mr-md-0">
                <li class="nav-item">
                    <a class="nav-link pr-0" id="logout">
                        <div class="media align-items-center">
                            <div class="media-body ml-3 d-lg-block">
                                <button class="btn btn-icon btn-secondary" type="button">
                                    <span class="btn-inner--icon"><i class="ni ni-circle-08 mr-2"></i></span>
                                    <span class="mb-0 text-sm font-weight-bold">
                                    Logout
                                    </span>
                                </button>
                            </div>
                        </div>
                    </a>
                </li>
            </ul>
        </div>
    </div>
</nav>

<?php if (isset($pengumuman->status) && $pengumuman->status  == 1) { ?>
    <!-- Modal -->
    <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document" style="max-width: 1090px;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLongTitle"> Pengumuman </h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <?= $pengumuman->pengumuman; ?>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"> Tutup </button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>