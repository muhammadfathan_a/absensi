<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {


    public function __construct()
	{
		parent::__construct();
		$this->load->model('EmailModel');
	}

    public function updateEmail(){
        $kodedosen = $this->session->userdata('kodedosen');
        $data['EMAIL'] = $this->input->post('email');
        $this->EmailModel->updateEmail($kodedosen,$data);

        echo "<script> 
        alert('Email Berhasil di Update!'); 
        window.location ='".base_url()."app/homepages'; 
        </script>";	
    }

}

/* End of file Email.php */
