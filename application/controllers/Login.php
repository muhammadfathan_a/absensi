<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
	$this->load->library('session');
	$this->load->model('LoginModel', 'LoginModel');
    }

    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('pages/login');
        $this->load->view('templates/footer');
    }

    public function check(){
        /* API URL */
		$url = base_url('util/api/login/dosen');
		/* Init cURL resource */
		$ch = curl_init($url);
		/* Array Parameter Data */
		$data = [
			'kodedosen'		=>	$this->input->post('kodedosen'),
			'pwd'	=>	$this->input->post('pwd')
		];
		/* pass encoded JSON string to the POST fields */
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		/* set return type json */
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		/* execute request */
		$result = curl_exec($ch);

		if ($result) {
            		$getItem = json_decode($result, false);
            		$er =	$getItem[0]->er; 
	    		if ($er == "") {
	 		$this->session->set_userdata([
				'kodedosen'	=>	$getItem[0]->kode_dosen,
                    		'namadosen'	=>	$getItem[0]->nama_dosen,
                    		'thakadkrs' =>  $getItem[0]->thakad,
                    		'semesterkrs' =>  $getItem[0]->SEMESTER
			]);
                	// // header('Location: ' . base_url('auth'));
                	// var_dump($getItem);
            		$this->load->view('alert/alert_benar');
                	echo "success";
            		}
            		else if($er == 'false'){
                		curl_close($ch);
                		echo "error";
            		}
		}
    }

    public function logout()
	{
		$this->session->sess_destroy();
		//echo '<script> window.location = "' . base_url('Login') . '"</script>';
		redirect('http://sso-ui.uhamka.ac.id/dashboard');
	}

    public function validate() {
		$username = htmlspecialchars($this->input->post('kodedosen', TRUE), ENT_QUOTES);
		$cek_users = $this->LoginModel->getPassword($username);	
//	var_dump($username); die();
		$konek = "";
		if ($cek_users->num_rows() > 0) {
			$data = $cek_users->row_array();
			$konek = $data['pass'];

			// echo "success";
			/* API URL */
			$url = base_url('util/api/login/dosen');
			/* Init cURL resource */
			$ch = curl_init($url);
			/* Array Parameter Data */
			$data = [
				'kodedosen'		=>	$username,
				'pwd'	=>	$konek
			];
			/* pass encoded JSON string to the POST fields */
			curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
			/* set return type json */
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
			/* execute request */
			$result = curl_exec($ch);

			if ($result) {
				$getItem = json_decode($result, false);
				$er =	$getItem[0]->er; 
				if ($er == "") {
					$this->session->set_userdata([
						'kodedosen'	=>	$getItem[0]->kode_dosen,
						'namadosen'	=>	$getItem[0]->nama_dosen,
						'thakadkrs' =>  $getItem[0]->thakad,
						'semesterkrs' =>  $getItem[0]->SEMESTER
					]);
					// // header('Location: ' . base_url('auth'));
					// print_r($getItem);
					echo "success";
				}
				else if($er == 'false'){
					curl_close($ch);
					echo "error";
				}
			}
		} else {
			echo "error";
		}

    }

}

