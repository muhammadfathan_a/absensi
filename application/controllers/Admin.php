<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin extends CI_Controller{

    public function __construct()
    {
        parent::__construct();
        $this->load->model('AdminModel');
    }

    public function login()
    {
        $this->load->view('templates/header');
        $this->load->view('pages/admin/login_admin');
        $this->load->view('templates/footer');
    }

    public function login_check()
    {
        if($this->input->post('username_admin')=='admin' && $this->input->post('pwd_admin')=='uhamka212'){
            redirect('/admin/pengumuman', 'location');
        }else{
            echo  "<script>alert('Username / Password Salah');document.location='login'</script>";
        }
    }
    
    public function pengumuman()
    {
        $data['pengumuman'] = $this->AdminModel->getPengumuman();
        
        $this->load->view('templates/header');
        $this->load->view('layouts/topnav_adm',$data);
        $this->load->view('pages/admin/pengumuman',$data);
    }

    public function save_pengumuman()
    {
        $id = $this->input->post('id_pengumuman');
        $pengumuman = $this->input->post('pengumuman');
        $status =  $this->input->post('status');

        if ($status == NULL) {
            $status = 0;
        } else if ($status == 'on') {
            $status = 1;
        }

        $this->AdminModel->savePengumuman($id, $pengumuman, $status);
        redirect(base_url() . 'admin/pengumuman');
    }
}

