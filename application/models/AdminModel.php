<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class AdminModel extends CI_Model { 
    
    // public function getPengumuman() {
    //     $res = $this->db->get('T_PENGUMUMAN_SIAP');

    //     return $res;
    // }

    // public function savePengumuman($id, $pengumuman, $status) {
    //     $this->db->set('pengumuman', $pengumuman);
    //     $this->db->set('status', $status);

    //     $this->db->where('id_pengumuman', $id);
    //     $this->db->update('T_PENGUMUMAN_SIAP');
    // }

    public function getPengumuman() {
        $db2 = $this->load->database('dbmysql', TRUE);
        $res = $db2->get('T_PENGUMUMAN_SIAP');

        return $res->row();
    }

    public function savePengumuman($id, $pengumuman, $status) {
        $db2 = $this->load->database('dbmysql', TRUE);
        $db2->set('pengumuman', $pengumuman);
        $db2->set('status', $status);

        $db2->where('id_pengumuman', $id);
        $db2->update('T_PENGUMUMAN_SIAP');
    }

}