<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class EmailModel extends CI_Model {

    public function getStatus($kodedosen){
        $this->db->where('KODEDOSEN',$kodedosen);
        $query = $this->db->get('T_DOSEN');

        if($query){
            return $query->row_array();
        }else{
            return false;
        }
    }

    public function updateEmail($kodedosen,$data){
        $this->db->set('isUpdateEmail', 1);
        $this->db->where('KODEDOSEN', $kodedosen);
        $query =  $this->db->update('T_DOSEN',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }

}

/* End of file EmailModel.php */
