<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class LoginController extends REST_Controller {
	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Model_login','modellogin');
	}

	public function index_post(){
		$nim = $this->post('kodedosen');
		$pwd = $this->post('pwd');


		$result = $this->modellogin->getLogin($nim,$pwd);

		if($result) {
			$this->response($result, 200);
		}
		else {
            $this->response($result['er'], 400);
		}
	}

	public function loginAdmin_post(){
		$username = $this->post('username');
		$password = $this->post('password');


		$result = $this->modellogin->loginAdmin($username,$password);

		if($result) {
			$this->response($result, 200);
		}
		else {
            $this->response([
				'er'=>false,
				'msg'=>'not found'
			], 400);
		}
	}

	public function akm_post(){
		$nim = $this->post('nim');


		$result = $this->modellogin->akm($nim);

		if($result) {
			$this->response($result, 200);
		}
		else {
            $this->response([
				'er'=>false,
				'msg'=>'not found'
			], 400);
		}
	}
}