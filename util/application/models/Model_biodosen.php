<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_biodosen extends CI_Model {

	public function getDosen($kodedosen){
		$this->db->select('KODEDOSEN,NAMADOSEN,KELAMIN,NIRD,NIP,GELAR,TMPLAHIR,TGLAHIR,UNIT,NAMAPROGDI,ALAMAT1,ALAMAT2,RT,KOTA,EMAIL,NIDN');
		$this->db->where('KODEDOSEN',$kodedosen);
		$query = $this->db->get('V_Android_Biodosen');
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}
}