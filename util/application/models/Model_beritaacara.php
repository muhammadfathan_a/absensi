<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_beritaacara extends CI_Model {

	public function getBeritaAcara($thakad,$semester,$dosen){

		$sp = "WSBeritaAcara ?,?,?";
		$param = array(
			'thakad'=> $thakad,
			'semester' => $semester,
			'kddosen' => $dosen
		);

		$result = $this->db->query($sp,$param);

		return $result->result();
	}

}