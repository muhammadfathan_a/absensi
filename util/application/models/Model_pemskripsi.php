<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pemskripsi extends CI_Model {

	public function getMahasiswa($kodedosen){
		$this->db->select('NIM,NAMA,NAMAPROGDI,THAKAD,SEMESTER,TGLUJISKRIPSI,DosenPemb1,DosenPemb2');
		$this->db->where('DosenPemb1',$kodedosen);
		$this->db->or_where('DosenPemb2',$kodedosen);
		$query = $this->db->get('V_Android_NilaiSkripsi');
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}
}