<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class BioDosen extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Model_biodosen','modelbiodosen');
	}

	public function dosen_get(){
		$kode = $this->get('kodedosen');

		if(!$kode) {
			$this->response(NULL,400);
		}

		$result = $this->modelbiodosen->getDosen($kode);

		if($result) {
			$this->response($result, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No data were found'
			], 404); 
		}
	}

}