<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class BeritaAcara extends REST_Controller {
	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Model_beritaacara','modelberitaacara');
	}

	public function berita_get(){
		$thakad = $this->get('thakad');
		$semester = $this->get('semester');
		$kd = $this->get('kodedosen');


		if(!$thakad && $semester && $kd) {
			$this->response(NULL,400);
		}

		$result = $this->modelberitaacara->getBeritaAcara($thakad,$semester, $kd);

		if($result) {
			$this->response($result, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No data were found'
			], 404); 
		}
	}
}