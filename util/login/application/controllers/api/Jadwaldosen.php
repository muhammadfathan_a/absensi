<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Jadwaldosen extends REST_Controller {
	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Model_jadwal','modeljadwal');
	}

	public function jadwalkode_get(){
		$kode = $this->get('kodedosen');

		if(!$kode) {
			$this->response(NULL,400);
		}

		$result = $this->modeljadwal->getJadwalKode($kode);

		if($result) {
			$this->response($result, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No data were found'
			], 404); 
		}
	}

	public function jadwalkodetahun_get(){
		$kode = $this->get('kodedosen');
		$thakad = $this->get('thakad');
		$semester = $this->get('semester');



		if(!$kode && !$thakad) {
			$this->response(NULL,400);
		}

		$result = $this->modeljadwal->getJadwalKodeTahun($kode,$thakad,$semester);

		if($result) {
			$this->response($result, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No data were found'
			], 404); 
		}
	}
}