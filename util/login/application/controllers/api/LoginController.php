<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class LoginController extends REST_Controller {
	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Model_login','modellogin');
	}

	public function index_post(){
		$nim = $this->post('nim');
		$pwd = $this->post('password');


		$result = $this->modellogin->getLogin($nim,$pwd);

		if($result) {
			$this->response($result, 200);
		}
		else {
            $this->response($result['er'], 400);
		}
	}
}