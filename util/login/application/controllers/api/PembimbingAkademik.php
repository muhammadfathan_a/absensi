<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class PembimbingAkademik extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Model_pemakademik','modelpemakademik');
	}

	public function mahasiswa_get(){
		$kode = $this->get('dosenpa');

		if(!$kode) {
			$this->response(NULL,400);
		}

		$result = $this->modelpemakademik->getMahasiswa($kode);

		if($result) {
			$this->response($result, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No data were found'
			], 404); 
		}
	}
}