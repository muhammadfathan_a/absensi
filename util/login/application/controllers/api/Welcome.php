<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class Welcome extends REST_Controller {

    public function welcomes_get()
    {
        $this->response([
            'status' => 'success',
            'message' => 'SIMPEG API'
        ], REST_Controller::HTTP_NOT_FOUND);
    }
}
