<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';

// use namespace
use Restserver\Libraries\REST_Controller;

class PembimbingSkripsi extends REST_Controller {

	public function __construct($config = 'rest')
	{
		parent::__construct($config);
		$this->load->model('Model_pemskripsi','modelpemskripsi');
	}

	public function mahasiswa_get(){
		$kode = $this->get('dosenpemb');

		if(!$kode) {
			$this->response(NULL,400);
		}

		$result = $this->modelpemskripsi->getMahasiswa($kode);

		if($result) {
			$this->response($result, 200);
		}
		else {
			$this->response([
			    'status' => FALSE,
			    'message' => 'No data were found'
			], 404); 
		}
	}

}