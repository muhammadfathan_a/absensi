<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_jadwal extends CI_Model {

	public function getJadwalKode($kodedosen){
		$this->db->select('THAKAD,SEMESTER,SINGKATAN,NAMAPROGDI,MATAKULIAH,SKS,KELAS,KODEDOSEN,NAMADOSEN,RUANG,KAMPUS,HARI,MULAI,SAMPAI');
		$this->db->where('KODEDOSEN',$kodedosen);
		$query = $this->db->get('V_JADWALDOSEN');
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}

	public function getJadwalKodeTahun($kodedosen,$tahun,$semester){
		$this->db->select('THAKAD,SEMESTER,SINGKATAN,NAMAPROGDI,MATAKULIAH,SKS,KELAS,KODEDOSEN,NAMADOSEN,RUANG,KAMPUS,HARI,MULAI,SAMPAI');
		$this->db->where('KODEDOSEN',$kodedosen);
		$this->db->where('THAKAD',$tahun);
		$this->db->where('SEMESTER',$semester);
		$query = $this->db->get('V_JADWALDOSEN');
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}
}