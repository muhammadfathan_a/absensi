<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_pemakademik extends CI_Model {

	public function getMahasiswa($kodedosen){
		$this->db->select('NIM,NAMA,KELAMIN,ANGKATAN,NAMAPROGDI,THAKADKELUAR,SEMESTERKELUAR,TGSKYUDIS');
		$this->db->where('DOSENPA',$kodedosen);
		$query = $this->db->get('V_Android_BioMHS');
		if($query){
			return $query->result();
		}else{
			return false;
		}
	}

}