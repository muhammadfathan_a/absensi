<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Model_login extends CI_Model {

	public function getLogin($nim,$pwd){

		$sp = "S_LOGINMHS_WISUDA ?,?";
		$param = array(
			'nim'=> $nim,
			'pwd' => $pwd
		);

		$result = $this->db->query($sp,$param);

		return $result->result();
	}

}