$('#logout').on('click', function(e) {
	Swal.fire({
		title: 'Anda yakin Keluar?',
		text: "Anda akan keluar dari halaman ini",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'Yes'
	}).then((result) => {
		if (result.value) {
				window.location.href = "../login/logout";
		}
	})

});
