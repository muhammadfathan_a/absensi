// declare url segmentation
var segment = window.location.pathname.split('/');
var segmentUrl = segment[segment.length-1].split('?');

console.log(segmentUrl);

// check absensi segment?
if (segmentUrl == 'dashboard') {
	// run function
	getMahasiswa(40);	
}

// declare function
function getMahasiswa(maxMahasiswa) {
	// first value
	var switchStatus = true;
	// attend and alpha
	var attend = maxMahasiswa;
	var alpha = 0;
	document.getElementById('attendSum').innerHTML = attend;
	document.getElementById('alphaSum').innerHTML = alpha;
	// loop as max data
	for (let index = 1; index <= maxMahasiswa; index++) {
		// check switch status
		if (switchStatus == true) {
			document.getElementById('attend-name_${index}').innerHTML += 'Hadir';
		}
		// action to change switch cheked or not and give the value
		$('#customSwitch${index}').on('change', function() {
			switchStatus = $(this).is(':checked');
			if (switchStatus == true) {
				document.getElementById('attend-name_${index}').innerHTML = 'Hadir';
				document.getElementById('attendSum').innerHTML = attend;
			}
			else if(switchStatus == false) {
				document.getElementById('attend-name_${index}').innerHTML = 'Absen';
				if (index) {
					document.getElementById('attendSum').innerHTML = alpha;
					console.log('${index} value ${alpha + 1}');
				}
			}
		});
	}
}
