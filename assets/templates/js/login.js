
$(document).ready(function() {

	$('#login-btn').on('click', function(e) {
		e.preventDefault();

		var kodedosen = $("#kodedosen").val();
		var pwd = $("#pwd").val();

		if (kodedosen.length == "") {

			Swal.fire({
				type: 'warning',
				icon: 'warning',
				title: 'Oops...',
				text: 'Kodedosen Wajib Diisi!',
				allowOutsideClick: false
			});

		} else if (pwd.length == "") {

			Swal.fire({
				type: 'warning',
				icon: 'warning',
				title: 'Oops...',
				text: 'Password Wajib!',
				allowOutsideClick: false
			});

		} else {

			$.ajax({

				url: "login/check",
				type: "POST",
				data: {
					"kodedosen": kodedosen,
					"pwd": pwd
				},

				success: function(response) {
					response = response.trim()

					if (response == "success") {

						Swal.fire({
							title: 'Berhasil Login, Selamat Datang',
							html: 'Anda akan di direct ke dashboard <b></b>.',
							timer: 2100,
							timerProgressBar: true,
							onBeforeOpen: () => {
								Swal.showLoading()
								timerInterval = setInterval(() => {
									const content = Swal.getContent()
									if (content) {
										const b = content.querySelector('b')
										if (b) {
											b.textContent = Swal.getTimerLeft()
										}
									}
								}, 100)
							},
							onClose: () => {
								clearInterval(timerInterval)
							}
						}).then((result) => {
							/* Read more about handling dismissals below */
							window.location.href = "app/homepages";
						});



					} else if (response == 'error') {
						Swal.fire({
							type: 'error',
							icon: 'error',
							title: 'Login Gagal!',
							text: ' Password atau Username salah!',
							allowOutsideClick: false
						});
					} else {
						Swal.fire({
							type: 'error',
							icon: 'error',
							title: 'Koneksi Bermasalah!',
							text: 'Koneksi bermasalah, Silahkan coba lagi',
							allowOutsideClick: false
						});
					}

					console.log(response);

				},

				error: function(response) {

					Swal.fire({
						type: 'error',
						title: 'Opps!',
						text: 'server error!'
					});

					console.log(response);

				}

			});

		}

	});

	var getUrlParameter = function getUrlParameter(sParam) {
		var sPageURL = window.location.search.substring(1),
			sURLVariables = sPageURL.split('&'),
			sParameterName,
			i;

		for (i = 0; i < sURLVariables.length; i++) {
			sParameterName = sURLVariables[i].split('=');

			if (sParameterName[0] === sParam) {
				return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
			}
		}
		return false;
	};
	
	var token = getUrlParameter('token');
	var kodedsn = getUrlParameter('kodedosen');

	if (token.length > 0) {
		
		var kodedosen = kodedsn;

		if (kodedosen.length == 0) {

			Swal.fire({
				type: 'warning',
				icon: 'warning',
				title: 'Oops...',
				text: 'Kodedosen Wajib Diisi!',
				allowOutsideClick: false
			});

		} else {

			$.ajax({

				url: "login/validate",
				type: "POST",
				data: {
					"kodedosen": kodedosen
				},

				success: function(response) {
					response = response.trim()
					console.log(response);

					if (response == "success") {

						Swal.fire({
							title: 'Berhasil Login, Selamat Datang',
							html: 'Anda akan di direct ke dashboard <b></b>.',
							timer: 100,
							timerProgressBar: true,
							onBeforeOpen: () => {
								Swal.showLoading()
								timerInterval = setInterval(() => {
									const content = Swal.getContent()
									if (content) {
										const b = content.querySelector('b')
										if (b) {
											b.textContent = Swal.getTimerLeft()
										}
									}
								}, 10)
							},
							onClose: () => {
								clearInterval(timerInterval)
							}
						}).then((result) => {
							/* Read more about handling dismissals below */
							window.location.href = "app/homepages";
						});



					} else if (response == 'error') {
						Swal.fire({
							type: 'error',
							icon: 'error',
							title: 'Login Gagal!',
							text: ' Password atau Username salah!',
							allowOutsideClick: false
						}).then(function() {
							window.location = "http://sso-ui.uhamka.ac.id/dashboard";
						});
					} else {
						Swal.fire({
							type: 'error',
							icon: 'error',
							title: 'Koneksi Bermasalah!',
							text: 'Koneksi bermasalah, Silahkan coba lagi',
							allowOutsideClick: false
						}).then(function() {
							window.location = "http://sso-ui.uhamka.ac.id/dashboard";
						});
					}

					console.log(response);

				},

				error: function(response) {
					console.log(response);

					Swal.fire({
						type: 'error',
						title: 'Opps!',
						text: 'server error!'
					}).then(function() {
						window.location = "http://sso-ui.uhamka.ac.id/dashboard";
					});

					// console.log(response);

				}

			});

		}

	}

});
