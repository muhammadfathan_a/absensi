<?php
defined('BASEPATH') or exit('No direct script access allowed');

class App extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('JadwalModel');
	}

	public function index()
	{
		$this->load->view('templates/header');
		$this->load->view('pages/welcome');
		$this->load->view('templates/footer');
	}

	public function dashboard()
	{
		if ($this->session->userdata('kodedosen')) {

			$idjadwal = $this->input->get('id');
			$pertemuan = $this->input->get('pertemuan');

			$data['JadwalIdentity'] = $this->JadwalModel->getJadwalIdentity($idjadwal);
			$data['isExist'] = $this->JadwalModel->getJadwalReal($idjadwal,$pertemuan);


			$this->load->view('templates/header');
			$this->load->view('layouts/topnav');
			$this->load->view('pages/dashboard',$data);
			$this->load->view('templates/footer');
		} else {
			echo '<script> 
				alert("Silahkan login terlebih dahulu"); 
				window.location = "' . base_url('Login') . '" 
			</script>';
		}
	}

	public function view_presensi($idjadwal) {
		$data['JadwalDetail'] = $this->JadwalModel->getJadwalDetail($idjadwal);
		
		// echo "<b>Heloo</b>";
		$this->load->view('pages/dashboard_detail',$data);
	}

	public function homepages()
	{
		if ($this->session->userdata('kodedosen')) {
			$kodedosen = $this->session->userdata('kodedosen');
			if($this->session->userdata('semesterkrs') == '10'){
				$semester = 'Ganjil';

			}else if($this->session->userdata('semesterkrs') == '20'){
				$semester = 'Genap';
			}

			$thakad = $this->session->userdata('thakadkrs');

			$hari = onlyday_indo( date('Y-m-d') );



			$data['dataJadwalAll'] = $this->JadwalModel->getJadwalAll($kodedosen, $thakad, $semester);
			$data['dataJadwalPerDay'] = $this->JadwalModel->getJadwalPerDay($kodedosen, $hari,  $thakad, $semester);


			// print_r($data); echo $kodedosen; die();

			$this->load->view('templates/header');
			$this->load->view('layouts/topnav');
			$this->load->view('pages/homepage',$data);
			$this->load->view('templates/footer');
		} else {
			echo '<script> 
				alert("Silahkan login terlebih dahulu"); 
				window.location = "' . base_url('Login') . '" 
			</script>';
		}
	}
}
