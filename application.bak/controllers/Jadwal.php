<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Jadwal extends CI_Controller{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('JadwalModel');
    }
    
    public function Absen(){
        $dataAjax = $this->input->post();

        $this->JadwalModel->updateAbsen($dataAjax);

        $idjadwal = $this->input->get('id');
        $data['JadwalDetail'] = $this->JadwalModel->getJadwalDetail($idjadwal);

        // $this->load->view('pages/dashboard',$data);
		$this->load->view('pages/dashboard_detail',$data);
    }

    public function Hadir(){
        $dataAjax = $this->input->post();

        $this->JadwalModel->updateHadir($dataAjax);

        $idjadwal = $this->input->get('id');
        
        $data['JadwalDetail'] = $this->JadwalModel->getJadwalDetail($idjadwal);

        // $this->load->view('pages/dashboard',$data);
		$this->load->view('pages/dashboard_detail',$data);
    }

    public function save(){

      $flag = '';
      $now = strtotime(date('H:i'));
      $start = strtotime('07:00');
      $mid = strtotime('12:00');

      if($now >= $start && $now <= $mid){
        $flag = 'P';
      }else if($now > $mid){
        $flag = 'S';
      }

      $data = array(
        'IDJADWAL'     => $this->input->post('idjadwal'),
        'PERTEMUAN'    => $this->input->post('pertemuan'),
        'KODEDOSEN'    => $this->session->userdata('kodedosen'),
        'TANGGAL'      => date('Y-m-d H:i:s'),
        'WaktuAbsen'   => date('H:i'),
        'PSFlag'       => $flag,
        'PokokBahasan' => $this->input->post('berita'),
        'InsertBy'     => '',
        'LastUpdateBy' => '',
        'Cronjob'      => '0',
        'Inputan'      => date('Y-m-d'),
      );

      $this->JadwalModel->insertBeritaAcara($data);

      echo "<script> 
      alert('Data Berhasil Disimpan!'); 
      window.location ='".base_url()."app/homepages'; 
      </script>";	
      
    }

}