<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('templates/header');
        $this->load->view('pages/login');
        $this->load->view('templates/footer');
    }

    public function check(){
        /* API URL */
		$url = base_url('util/api/login/dosen');
		/* Init cURL resource */
		$ch = curl_init($url);
		/* Array Parameter Data */
		$data = [
			'kodedosen'		=>	$this->input->post('kodedosen'),
			'pwd'	=>	$this->input->post('pwd')
		];
		/* pass encoded JSON string to the POST fields */
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		/* set return type json */
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		/* execute request */
		$result = curl_exec($ch);

		if ($result) {
            $getItem = json_decode($result, false);
            $er =	$getItem[0]->er; 
			if ($er == "") {
				$this->session->set_userdata([
					'kodedosen'	=>	$getItem[0]->kode_dosen,
                    'namadosen'	=>	$getItem[0]->nama_dosen,
                    'thakadkrs' =>  $getItem[0]->thakad,
                    'semesterkrs' =>  $getItem[0]->SEMESTER
				]);
                // // header('Location: ' . base_url('auth'));
                // print_r($getItem);
                echo "success";
            }
            else if($er == 'false'){
                curl_close($ch);
                echo "error";
            }
		}
    }

    public function logout()
	{
		$this->session->sess_destroy();
		echo '<script> window.location = "' . base_url('Login') . '"</script>';
		// redirect('app');
	}

}
