<?php 
defined('BASEPATH') OR exit('No direct script access allowed'); ?>

<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Start your development with a Dashboard for Bootstrap 4.">
	<meta name="author" content="Creative Tim">
	<title> Absensi Mahasiswa | UHAMKA &mdash; <?= date('Y') ?> </title>

	<link rel="icon" href="<?= base_url('assets/templates/img/brand/favicon.png') ?>" type="image/png">

	<link rel="stylesheet" href="<?= base_url('assets/templates/vendor/nucleo/css/nucleo.css') ?>" type="text/css">
	<link rel="stylesheet" href="<?= base_url('assets/templates/vendor/@fortawesome/fontawesome-free/css/all.min.css') ?>" type="text/css">

	<link rel="stylesheet" href="<?= base_url('assets/templates/css/argon.css?v=1.1.0') ?>" type="text/css">
	<link rel="stylesheet" href="<?= base_url('assets/css/style.css') ?>">
</head>
<body>
    <!-- Main content -->
    <div class="main-content">
