<body style="
	background-image: url(<?= base_url('assets/img/bg.png') ?>); 
	background-repeat: no-repeat; 
	background-position: center; 
	background-attachment: fixed;
	height: 100%; 
	background-size: cover;
">
    <!-- login content -->
    <div class="login-content py-7">
        <!-- Page content -->
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-5 col-md-7">
					<div class="card border-0 mb-0" style="
						/* background-image: url(<?= base_url('assets/img/bg.png') ?>);  */
						height:100%; background-position:center; 
						border-radius:15px; 
						background-repeat: no-repeat; 
					">
                        <div class=" card-body px-lg-5 py-lg-5">
                            <div class="text-center text-dark mb-4 mt--4">
                                <div class="text-center">
                                    <img class="mb-3" src="<?= base_url('assets/img/UHAMKA/logo1.png') ?>" width="210" alt="">
                                    <p class="text-primary" style="font-size:21px;">
										<span style="font-weight:600;"> Sistem </span>
										<span> Absensi Mahasiswa </span>
									</p>
                                </div>
                            </div>
                            <form role="form" class="mt-5">
								<p class="text-center"> silakan login terlebih dahulu </p>
                                <div class="form-group mb-3">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-circle-08"></i></span>
                                        </div>
                                        <input class="form-control text-primary" placeholder="Kode Dosen" type="text" name="kodedosen" id="kodedosen">
                                    </div>
                                </div>
                                <div class="form-group mb-5">
                                    <div class="input-group input-group-merge input-group-alternative">
                                        <div class="input-group-prepend">
                                            <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                                        </div>
                                        <input class="form-control text-primary" placeholder="Password" type="password" name="pwd" id="pwd">
                                    </div>
                                </div>
                                <div class="text-center">
                                    <button id="login-btn" type="submit" class="btn btn-primary btn-lg btn-block">
                                        Login <i class="fas fa-arrow-right ml-1"></i>
                                    </button>
                                </div>
                            </form>
                        </div>
						<div class="text-center mt-2 mb-4">
							Craft with <i class="fas fa-heart text-danger"></i> By. 
							<a href="https://bpti.uhamka.ac.id/internship-program/" target="_blank">
								Intership BPTI
							</a>
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
