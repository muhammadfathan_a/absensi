<?php foreach($JadwalDetail as $jadwaldetail) { ?>
    <div class="col-xl-3 col-md-6 col-sm-12 ">
        <div class="card card-primary px-0 rounded card-stats shadow" style="margin-bottom: 15px;">
            <!-- Card body -->
            <div class="card-body px-0">
                <div class="row" style="width: 100%; margin: auto; zoom: 110%;">
                    <!-- atas -->
                    <div class="col-sm-12 col-md-12">
                        <h4 class="capitalize font-weight-500 mb-2">
                            <?= $jadwaldetail['NAMA'] ?>
                        </h4>
                    </div>
                    <!-- bawah -->
                    <div class="row d-flex" style="width: 100%; margin: auto;">
                        <div class="col-sm-12 col-md-6">
                            <h5 class="text-mute mb-0 font-weight-300">
                            <?= $jadwaldetail['NIM'] ?>
                            </h5>
                        </div>
                        

                        <?php if( $jadwaldetail['K'.$jadwaldetail['JmlAbsen']] == '0') { ?>
                            <div class="col-sm-12 col-md-6 text-right float-right">
                                <div class="custom-control custom-switch" style="padding-left: 2.5rem; zoom: 90%;">
                                    <input type="checkbox" name="attend_type_<?= $jadwaldetail['NIM'] ?>" class="custom-control-input bg-success btn-absen" data-nim="<?= $jadwaldetail['NIM'] ?>" data-idjadwal="<?= $jadwaldetail['IDJADWAL'] ?>" checked id="customSwitch_<?= $jadwaldetail['NIM'] ?>">
                                    <label class="custom-control-label" for="customSwitch_<?= $jadwaldetail['NIM'] ?>">
                                        <small class="mx-2">Hadir</small>
                                    </label>
                                </div>
                            </div>
                        <?php }else if($jadwaldetail['K'.$jadwaldetail['JmlAbsen']] == '1') { ?>
                            <div class="col-sm-12 col-md-6 text-right float-right">
                                <div class="custom-control custom-switch" style="padding-left: 2.5rem; zoom: 90%;">
                                    <input type="checkbox" name="attend_type_<?= $jadwaldetail['NIM'] ?>" class="custom-control-input bg-success btn-hadir" data-nim="<?= $jadwaldetail['NIM'] ?>" data-idjadwal="<?= $jadwaldetail['IDJADWAL'] ?>" id="customSwitch_<?= $jadwaldetail['NIM'] ?>">
                                    <label class="custom-control-label" for="customSwitch_<?= $jadwaldetail['NIM'] ?>">
                                        <small class="mx-2">Tidak Hadir</small>
                                    </label>
                                </div>
                            </div>

                        <?php } ?>

                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<script>
		$('.btn-absen').on('change', function(e) {
			// e.preventDefault();
		
			let stat = true;
            let idjadwal, nim;
            let pertemuan;
            
            pertemuan = "<?= $this->input->get('pertemuan') ?>"

			nim = $(this).data('nim');
			idjadwal = $(this).data('idjadwal');

			if(nim === '' || idjadwal === '') {
				stat = false;
			}

			console.log(stat);

			if (stat) {
				$.ajax({
					type : "POST",
					url  : "<?php echo base_url('Jadwal/Absen?pertemuan=')?>"+pertemuan,
					dataType : "html",
					data :"nim="+ nim+"&idjadwal="+idjadwal,
					success: function(data){
						$("#data-absen").load("<?php echo base_url('App/view_presensi/')?>"+idjadwal+"?pertemuan="+pertemuan)
						console.log(data);

					},
					error: function(xhr, stat, err){
						$("#data-absen").load("<?php echo base_url('App/view_presensi/')?>"+idjadwal+"?pertemuan="+pertemuan)

						console.log(err);
					},
					beforeSend : function(xhr, stat, err) {
						$("#data-absen").load("<?php echo base_url('App/view_presensi/')?>"+idjadwal+"?pertemuan="+pertemuan)

					}
				});
			}

		});
	</script>

<script>
		$('.btn-hadir').on('change', function(e) {
			// e.preventDefault();
		
			let stat = true;
            let idjadwal, nim;
            let pertemuan;
            
            pertemuan = "<?= $this->input->get('pertemuan') ?>"

			nim = $(this).data('nim');
			idjadwal = $(this).data('idjadwal');

			if(nim === '' || idjadwal === '') {
				stat = false;
			}

			console.log(stat);

			if (stat) {
				$.ajax({
					type : "POST",
					url  : "<?php echo base_url('Jadwal/Hadir?pertemuan=')?>"+pertemuan,
					dataType : "html",
					data :"nim="+ nim+"&idjadwal="+idjadwal,
					success: function(data){
						$("#data-absen").load("<?php echo base_url('App/view_presensi/')?>"+idjadwal+"?pertemuan="+pertemuan)
						console.log(data);

					},
					error: function(xhr, stat, err){
						$("#data-absen").load("<?php echo base_url('App/view_presensi/')?>"+idjadwal+"?pertemuan="+pertemuan)

						console.log(err);
					},
					beforeSend : function(xhr, stat, err) {
						$("#data-absen").load("<?php echo base_url('App/view_presensi/')?>"+idjadwal+"?pertemuan="+pertemuan)

					}
				});
			}

		});
	</script>