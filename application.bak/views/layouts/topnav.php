<!-- Topnav -->
<nav class="navbar navbar-top navbar-expand navbar-dark bg-primary border-bottom">
    <div class="container-fluid">
        <div class="collapse navbar-collapse float-right" id="navbarSupportedContent">
            <ul class="navbar-nav align-items-center mx-auto ml-md-0">
                <li class="nav-item">
                    <a class="nav-link px-0 text-white" href="<?= base_url('app/homepages') ?>" style="font-size: 16px;">
                        <span style="font-weight: 600;">
                            Sistem
                        </span>
                        <span style="font-weight: 100;">
                            Absensi Mahasiswa
                        </span>
                    </a>
                </li>
            </ul>
            <ul class="navbar-nav align-items-center mx-auto mr-md-0">
                <li class="nav-item dropdown">
                    <a class="nav-link pr-0" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                        <div class="media align-items-center">
                            <!-- <span class="avatar avatar-sm rounded-circle">
                                <img alt="Image placeholder" 
								src="<?php echo base_url('/assets/templates/img/theme/team-4.jpg'); ?>">
                            </span> -->
                            <div class="media-body ml-3 d-lg-block">
                                <button class="btn btn-icon btn-secondary" type="button">
                                    <span class="btn-inner--icon"><i class="ni ni-circle-08 mr-2"></i></span>
                                    <span class="mb-0 text-sm font-weight-bold">
                                        <?= $this->session->userdata('namadosen'); ?>
                                    </span>
                                </button>
                            </div>
                        </div>
                    </a>
                    <div class="dropdown-menu dropdown-menu-right">
                        <!-- <div class="dropdown-divider"></div> -->
                        <a class="dropdown-item">
                            <i class="ni ni-user-run"></i>
                            <span id="logout" type="submit">Logout</span>
                        </a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
</nav>