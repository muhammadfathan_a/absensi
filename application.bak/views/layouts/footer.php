<footer class="footer py-2 px-4 bg-primary rounded">
	<div class="row align-items-center justify-content-lg-between">
		<div class="col-lg-6">
			<div class="copyright text-center text-lg-left text-white">
				Copyright &copy; <?= date('Y') ?> 
				<a href="https://bpti.uhamka.ac.id/internship-program/" 
				class="font-weight-500 ml-1 text-white" target="_blank">
					Intership BPTI
				</a>
			</div>
		</div>
		<div class="col-lg-6">
			<ul class="nav nav-footer justify-content-center justify-content-lg-end">
				<li class="nav-item">
					<a href="https://www.uhamka.ac.id" class="nav-link" target="_blank" 
					style="color: #fff !important;">
						UHAMKAID
					</a>
				</li>
				<li class="nav-item">
					<a href="http://akademik.uhamka.ac.id" class="nav-link" target="_blank"
					style="color: #fff !important;">
						SIAKAD
					</a>
				</li>
				<li class="nav-item">
					<a href="https://onlinelearning.uhamka.ac.id" class="nav-link" target="_blank"
					style="color: #fff !important;">
						ONLINE LEARNING
					</a>
				</li>
			</ul>
		</div>
	</div>
</footer>
