<?php


defined('BASEPATH') OR exit('No direct script access allowed');

class JadwalModel extends CI_Model {

    public function getJadwalAll($kodedosen,$thakad,$semester){
        $this->db->select('*');
        $this->db->where('KODEDOSEN',$kodedosen);
        $this->db->where('THAKAD',$thakad);
        $this->db->where('SEMESTER',$semester);
        $this->db->order_by('HARI','DESC');
        $query = $this->db->get('V_Android_jadwal');


        $result = $query->result_array();

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    public function getJadwalIdentity($idjadawal){
        $this->db->select('*');
        $this->db->where('IDJADWAL',$idjadawal);
        $query = $this->db->get('V_Android_jadwal');

        $result = $query->result_array();

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    public function getJadwalDetail($idjadawal){
        $this->db->select('*');
        $this->db->where('IDJADWAL',$idjadawal);
        $query  = $this->db->get('V_Android_ABSEN');

        $result = $query->result_array();

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    public function getJadwalPerDay($kodedosen, $hari, $thakad, $semester){
        $this->db->select('*');
        $this->db->where('KODEDOSEN',$kodedosen);
        $this->db->where('THAKAD',$thakad);
        $this->db->where('SEMESTER',$semester);
        $this->db->where('HARI',$hari);
        // $this->db->order_by('HARI','DESC');
        $query = $this->db->get('V_Android_jadwal');
        
        $result = $query->result_array();

        if($result){
            return $result;
        }else{
            return false;
        }
    }


    public function getJadwalReal($idjadwal, $pertemuan){
        $this->db->select('*');
        $this->db->where('IDJADWAL',$idjadwal);
        $this->db->where('PERTEMUAN',$pertemuan);

        $query = $this->db->get('T_JADWALREAL');

        $result = $query->result_array();

        if($result){
            return $result;
        }else{
            return false;
        }
    }

    public function updateAbsen($payload){
        $pertemuan = $this->input->get('pertemuan');
        $this->db->set('K'.$pertemuan, 1);
        $this->db->where('NIM', $payload['nim']);
        $this->db->where('IDJADWAL', $payload['idjadwal']);
        $query =  $this->db->update('V_Android_ABSEN');

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function updateHadir($payload){
        $pertemuan = $this->input->get('pertemuan');
        $this->db->set('K'.$pertemuan, 0);
        $this->db->where('NIM', $payload['nim']);
        $this->db->where('IDJADWAL', $payload['idjadwal']);
        $query =  $this->db->update('V_Android_ABSEN');

        if($query){
            return true;
        }else{
            return false;
        }
    }

    public function insertBeritaAcara($data){
        $query = $this->db->insert('T_JADWALREAL',$data);

        if($query){
            return true;
        }else{
            return false;
        }
    }
}

/* End of file JadwalModel.php */
